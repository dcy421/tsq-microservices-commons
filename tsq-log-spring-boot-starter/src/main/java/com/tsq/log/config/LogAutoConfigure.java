package com.tsq.log.config;

import com.tsq.log.properties.TraceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @Author：dcy
 * @Description: 日志自动配置
 * @Date: 2019/9/19 9:30
 */
@EnableConfigurationProperties(TraceProperties.class)
public class LogAutoConfigure {
}
